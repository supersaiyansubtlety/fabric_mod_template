package net.sssubtlety.mod_id;

import net.fabricmc.api.ModInitializer;

public class Init implements ModInitializer {
    @Override
    public void onInitialize() {
        FeatureControl.init();
    }
}
